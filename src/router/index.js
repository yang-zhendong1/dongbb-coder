import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);
const routes = [
    {
      path: '/',
      name: 'HomePage',
      component: () => import('../views/HomePage.vue')
    },
    {
      path: '/database',
      name: 'DataBase',
      component: () => import('../views/DataBase.vue')
    },
    {
      path: '/project',
      name: 'Project',
      component: () => import('../views/Project.vue')
    },
    {
      path: '/generator',
      name: 'Generator',
      component: () => import('../views/Generator.vue')
    },
    {
      path: '/template',
      name: 'Template',
      component: () => import('../views/Template.vue')
    },
    {
      path: '/column-config',
      props: true,
      name: 'TableColumnConfig',
      component: () => import('../views/TableColumnConfig.vue')
    }
];

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
});

export default router
