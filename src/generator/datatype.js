export let dbJavaTypeRefer =  {
    //mysql
    "bigint":"java.lang.Long",
    "int":"java.lang.Integer",
    "tinyint":"java.lang.Boolean",
    "smallint":"java.lang.Integer",
    "mediumint":"java.lang.Integer",
    "varchar":"java.lang.String",
    "char": "java.lang.String",
    "text": "java.lang.String",
    "float": "java.lang.Float",
    "double": "java.lang.Double",
    "decimal": "java.math.BigDecimal",
    "datetime": "java.util.Date",
    "timestamp": "java.util.Date",

    //oracle
    "NUMBER":"java.lang.Long",
    "VARCHAR2":"java.lang.String",
    "NVARCHAR2":"java.lang.String",
    "CHAR": "java.lang.String",
    "DATE": "java.util.Date",
    "TIMESTAMP(0)": "java.util.Date",
    "TIMESTAMP(1)": "java.util.Date",
    "TIMESTAMP(2)": "java.util.Date",
    "TIMESTAMP(3)": "java.util.Date",
    "TIMESTAMP(4)": "java.util.Date",
    "TIMESTAMP(5)": "java.util.Date",
    "TIMESTAMP(6)": "java.util.Date",
    "TIMESTAMP(7)": "java.util.Date",
    "TIMESTAMP(8)": "java.util.Date",
    "TIMESTAMP(9)": "java.util.Date"
};