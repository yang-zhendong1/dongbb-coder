import db from '../utils/datastore'
import Databases from '../utils/databases'
import {toCamel,firstToUpperCamel} from '../utils/commons'
import {dbJavaTypeRefer} from '../generator/datatype'
import {elementUIValidStr} from '../generator/datatype-validator'

class OnceData {

    constructor(templateId,dbConfig,dbPwd,dbTable) {
        return new Promise((resolve, reject) => {
            if (!db.has(dbTable.tableName + '_column_config').value()) {
                new Databases(
                    dbConfig.dbType,
                    dbConfig.dbHost,
                    dbConfig.dbPort,
                    dbConfig.dbUser,
                    dbPwd,
                    dbConfig.dbName,
                ).getColumnInfo(dbTable.tableName).then(results => {
                    this.setBaseInfo(results,dbTable,templateId);
                    resolve(this);
                }).catch(error =>{
                    reject(error)
                })
            }else{ //如果有配置从配置里面查询，否则从数据库里面查询
                let results = db.read().get(dbTable.tableName + '_column_config').value()
                this.setBaseInfo(results,dbTable,templateId);
                resolve(this);
            }
        })
    }

    setBaseInfo(results,dbTable,templateId){
        this.columns = results
        this.tableName = dbTable.tableName
        this.tableComment = dbTable.tableComment
        this.templateInfo = db.read().get('template_config')
            .find({id: templateId})
            .value()
        if(this.templateInfo.otherInfo){
            this.templateInfo.otherInfo = JSON.parse(this.templateInfo.otherInfo);
        }
        this.projectInfo = db.read().get('project_config')
            .find({id: this.templateInfo.projectId})
            .value()
    }

    //将数据库表名(小写)转成驼峰命名，并且首字母大写
    get capitalizeCamelTableName() {
        return firstToUpperCamel(this.tableName.toLowerCase());
    }

    //将数据库表名(小写)转成驼峰命名,首字母是小写的
    get camelTableName() {
        return toCamel(this.tableName.toLowerCase())
    }

    //数据库表名(小写)，去掉下划线
    get flatTableName() {
        return this.tableName.toLowerCase().replace('_','')
    }

    //数据库字段名(小写)，转成驼峰命名，并给出各种编程语言对应的数据类型
    get camelFields(){
        return this.columns.map((column) => {
            column.columnName = toCamel(column.columnName.toLowerCase())
            column.columnNameUpperCase = firstToUpperCamel(column.columnName)
            column.columnJavaType = dbJavaTypeRefer[column.columnType]
            return column;
        })
    }

    get queryParamFields(){
        return this.columns.filter((column) => {
            return column.isQueryParam  === true;
        })
    }

    get showInTableFields(){
        return this.columns.filter((column) => {
            return column.showInTable  === true;
        })
    }

    get addEditedFields(){
        return this.columns.filter((column) => {
            return column.addEdited  === true;
        }).map((column) => {
            column.elementUIValidStr = elementUIValidStr(column)
            return column;
        })
    }

}

export default OnceData