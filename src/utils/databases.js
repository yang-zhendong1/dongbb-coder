import mysql from 'mysql';
//import oracledb from 'oracledb';

class Databases {
    constructor(dbType, host, port, dbUser, dbPwd, dbName) {

        if (dbType === "oracle") {
            /*this.oracleConfig = {
                user: dbUser,
                password: dbPwd,
                connectString: host + ":" + port + "/" + dbName
            }*/
        } else if (dbType === "mysql") {
            this.connection = mysql.createConnection({
                host: host,
                port: port,
                user: dbUser,
                password: dbPwd,
                database: dbName
            });
        }

        this.dbType = dbType;
        this.dbName = dbName
    }

    getTableInfo() {
        return new Promise((resolve, reject) => {
            if(this.dbType === "oracle"){
                //this.getTableInfoOracle(resolve, reject);
            }else if(this.dbType === "mysql"){
                this.getTableInfoMySQL(resolve, reject);
            }
        })
    }

    getColumnInfo(tableName) {
        return new Promise((resolve, reject) => {
            if(this.dbType === "oracle"){
                //this.getColumnInfoOracle(tableName,resolve, reject);
            }else if(this.dbType === "mysql"){
                this.getColumnInfoMySQL(tableName,resolve, reject);
            }
        })
    }

    /**
     * 获取MySQL的数据库表信息
     */
    getTableInfoMySQL(resolve, reject) {
        this.connection.connect(err => {
            if(err) return reject("数据库连接失败，请检查数据库连接！")
        });
        let sql = `SELECT table_name tableName,table_comment tableComment
                    FROM information_schema.tables
                    WHERE table_schema = ? 
                    AND (table_type = 'base table' OR table_type = 'BASE TABLE')`;

        this.connection.query(sql, this.dbName,
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    console.log(JSON.stringify(results))
                    return resolve(JSON.parse(JSON.stringify(results)))
                }
            })
        this.connection.end(function (err) {
            if(err) return reject("数据库关闭失败！")
        })
    }

    /**
     * 获取MySQL的数据库表字段信息
     */
    getColumnInfoMySQL(tableName, resolve, reject) {
        this.connection.connect(err => {
            if(err) return reject("数据库连接失败，请检查数据库连接！")
        });
        let sql = `SELECT  ordinal_position orderArrange,table_schema dbName,table_name tableName,
                   column_name columnName,data_type columnType,is_nullable isNullable,
                   character_maximum_length maximumLength,column_comment columnComment
            FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE table_name = ? 
            AND table_schema = ? 
            ORDER BY table_schema,table_name,ordinal_position`;

        this.connection.query(sql,[tableName,this.dbName],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(JSON.parse(JSON.stringify(results)))
                }
            })
        this.connection.end(function (err) {
            if(err) return reject("数据库关闭失败！")
        })
    }


    /**
     * 获取Oracle的数据库表信息
     */
    /*getTableInfoOracle(resolve, reject) {

        let sql = `SELECT a.TABLE_NAME tableName, b.COMMENTS tableComment
                   FROM USER_TABLES a
                   JOIN USER_TAB_COMMENTS b ON a.TABLE_NAME = b.TABLE_NAME`;

        oracledb.getConnection(this.oracleConfig).then(conn => {
            if(!conn){
                return reject("数据库连接失败，请检查数据库连接！")
            }
            conn.execute(sql, (error1, result) => {
                conn.close().then(r => {
                    return reject("数据库关闭失败！")
                });

                let tempObj;
                let retArray = [];
                result.rows.map(e => {
                    tempObj = {};
                    tempObj.tableName = e[0];
                    tempObj.tableComment = e[1];
                    retArray.push(tempObj);
                });
                return resolve(JSON.parse(JSON.stringify(retArray)));
            });
        });

    }*/


    /**
     * 获取MySQL的数据库表字段信息
     */
    /*getColumnInfoOracle(tableName, resolve, reject) {

        let sql = `SELECT a.column_id orderArrange,a.table_name  tableName,
                          a.column_name columnName, a.data_type columnType, a.nullable isNullable,
                          a.data_length maximumLength, b.comments columnComment
                   FROM user_tab_columns a,user_col_comments b
                   WHERE a.table_name =b.table_name
                     AND a.column_name  = b.column_name
                     AND a.table_name = :tableName
                   ORDER BY a.table_name ,a.column_id`;

        oracledb.getConnection(this.oracleConfig).then(conn => {
            if(!conn){
                return reject("数据库连接失败，请检查数据库连接！")
            }
            conn.execute(sql, [tableName],(error1, result) => {
                conn.close().then(r => {
                    return reject(error1 + ",并尝试数据库关闭失败！")
                });

                let tempObj;
                let retArray = [];
                result.rows.map(e => {
                    tempObj = {};
                    tempObj.orderArrange = e[0];
                    tempObj.tableName = e[1];
                    tempObj.columnName = e[2];
                    tempObj.columnType = e[3];
                    tempObj.isNullable = e[4];
                    tempObj.maximumLength = e[5];
                    tempObj.columnComment = e[6];
                    retArray.push(tempObj);
                });
                return resolve(JSON.parse(JSON.stringify(retArray)));
            });
        });
    }*/

}


export default Databases

